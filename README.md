# Verification of algebraic Riccati equations

These files contain methods to compute a verified enclosure of the stabilizing solution of an algebraic Riccati equation.

To run the methods, you will need to install:

* Intlab V. 6. Later versions probably work, too.
* pgdoubling from https://bitbucket.org/fph/pgdoubling and Matlab-xunit from http://au.mathworks.com/matlabcentral/fileexchange/22846-matlab-xunit-test-framework if you want to run the `*_permuted` methods.
* The verification code Mn.m from S. Miyajima at http://www1.gifu-u.ac.jp/~miyajima/CARE.zip if you want to run `Mn_wrapper`.
* CAREX from https://www-user.tu-chemnitz.de/~benner/pub/Software/CAREX/carex_m.tar.gz if you want to run the experiments taken from this suite.

All these packages need to be installed in a folder which is to be included in the Matlab path. For instance, initialize with
```
addpath('path_to_pgdoubling')
addpath('path_to_xunit/xunit')
addpath('path_to_carex')
addpath('path_to_Miyajima_CARE')
startintlab
```

and then re-run our experiments with
```
experiments_all_ChuLMcarex({@verifycare,@verifycare_hat_permuted_replace,@Mn_wrapper,@verifycare_fixedpoint_permuted})
experiments_scaled_carex(15, {@verifycare,@verifycare_hat_permuted_replace,@Mn_wrapper,@verifycare_fixedpoint_permuted})
```

You can configure the method used to solve CAREs in floating-point arithmetic by editing `care_solution.m`, for instance, to use `solveCARE_Miyajima` instead of `solveCARE_schur`.

## Authors
* Tayyebe Haqiri (S. Bahonar University, Kerman, Iran), thaqiri _at_ gmail.com
* Federico Poloni (University of Pisa, Italy), federico.poloni _at_ unipi.it
