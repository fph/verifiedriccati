function X = care_solution(A,G,Q)
% solves a CARE.
X = solveCARE_schur(A,G,Q);