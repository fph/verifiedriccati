function  g = garp(X)
%computes the geometric average relative precision of an interval matrix
%X
if any(isnan(X(:)))
    g = nan;
    return
end
Xrp = min(relerr(X),1);
index = find(~in(0,X));
temp = sum(sum(log(Xrp(index))))/length(index);
g = exp(temp);

end

