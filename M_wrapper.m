function [Xint,k,stab] = M_wrapper(A,G,Q)
% wrapper around the M() method by Miyajima, to return result consistently
% with the signature of the other functions.
[X,err,stab] = M(A,G,Q);
if not(isfinite(err))
    error('intlab:VerificationFailed','verification failed');
end
Xint = midrad(X,err);
k = nan;