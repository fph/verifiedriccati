function [XX, k, stab] = verifycare_fixedpoint_changebasis_mod(AA, GG, QQ)
% verify the stabilizing solution of an algebraic Riccati equation 
% A^*X + XA + Q = XGX using a fixed-point algorithm inspired by the ADI iteration.
%
% [XX, k, stab] = verifycare(A, G, Q)
%
% Returns: XX (intval): an enclosure for a solution. This is guaranteed to
% be an enclosure for the unique stabilizing solution of the CARE if
% stab=1.
% k: number of iterations taken
% stab: result of stability verification. Return values are as in
% verhurwstab.m from Intlab: 1 means that stability is verified, 0 or -1
% means failure.

Xtilde = care_solution(AA, GG, QQ);
n = length(AA);

Xtilde = intval(Xtilde);

Atilde = AA - GG*Xtilde;

Qtilde = AA'*Xtilde + Xtilde*Atilde + QQ;

% shift value (approximate/heuristic)
d = eig(mid(Atilde));
s = -min(real(d));
% construct a change of variables that tries to make Atilde close to
% normal
[P,T] = schur(mid(Atilde),'complex');
%[P,T] = schur(mid(Atilde),'real');
%eps = 1;
%for i = 1:n-1
%    eps = min(eps,(1/abs(diag(T,1)))^(1/i));
%end
%eps = 0.1;
%D = diag([1;cumprod(eps*ones(n-1,1))]);
D = eye(n); %TODO
V = P*D;
IV = verifylss(V,eye(size(V)));
VA = IV*Atilde*V;
VQ = V'*Qtilde*V;
VG = IV*GG*IV';

iVAtildec = verifylss(VA' - s*eye(n),eye(n));
VAplus = VA + s*eye(n);

%norm(mid(VAplus))*norm(mid(iVAtildec))

%VZ = hull(-iVAtildec*VQ*infsup(0.9,1.1)+realmin*infsup(-1,1),0);
VZ = -iVAtildec*VQ;

k=0;
while true
    VZ = hull(VZ*infsup(0.9,1.1)+realmin*infsup(-1,1),0);
    k = k + 1;
    if k == 50
         error('intlab:VerificationFailed','Verification failed');
    end
    VZZ = iVAtildec*(-VQ-VZ*(VAplus-VG*VZ));
%    VZZ = (VA'-VG*VZ-s*eye(n))\(-VQ-VZ*VAplus);

    ins = in0(VZZ,VZ);
    if all(ins(:))
        break
    end
%    Zbounds = [inf(VZ),sup(VZ)]
%    ZZbounds = [inf(VZZ),sup(VZZ)]
    VZ = VZZ;
%    VZ = intersect(VZ,VZZ);
end
XX= Xtilde + IV'*VZ*IV;

if nargout > 2
   stab = verhurwstab_miyajima(AA-GG*XX);
end
end