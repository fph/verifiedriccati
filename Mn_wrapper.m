function [Xint,k,stab] = Mn_wrapper(A,G,Q)
% wrapper around the Mn() method by Miyajima, to return result consistently
% with the signature of the other functions.
[X,err,stab] = Mn(A,G,Q,3,0.1);
if not(isfinite(err))
    error('intlab:VerificationFailed','verification failed');
end
Xint = midrad(X,err);
k = nan;
