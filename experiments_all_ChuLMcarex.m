function [nres, garps, conds, sizes, times, itrs, stabs] = experiments_all_ChuLMcarex(methods)
% Routine to make experiments on all problems in [Chu, Liu, Mehrmann]
%
% [nres, garps, conds, sizes, times, itrs, stabs] = experiments_all_ChuLMcarex(methods)
%
% methods: a cell array containing pointers to functions, e.g.
% methods = {@verifycare, @verifycare_hat_permuted_replace, @Mn_wrapper,
% @verifycare_fixedpoint_permuted}.

nExps = 33;
nres = nan(nExps,length(methods));
garps = nan(nExps,length(methods));
conds = nan(nExps,length(methods));
sizes = nan(nExps,length(methods));
times = nan(nExps,length(methods));
itrs = nan(nExps,length(methods));
stabs = nan(nExps,length(methods));

for i = 1:nExps %TODO
    [A,G,Q] = ChuLM07Carex(i);
    fprintf('%d',i);
    for j = 1:length(methods)
        f = methods{j};
        try
            sizes(i,j) = length(A);
            tic
            [IXX, k, stab] = f(A,G,Q);
            times(i,j) = toc;
            garps(i,j) = garp(IXX);
            nres(i,j) = nre(IXX);
            itrs(i,j) = k;
            stabs(i,j) = stab;
        catch err
            if strcmp(err.identifier,'intlab:VerificationFailed')
                continue
            else
                rethrow(err);
            end
        end
        fprintf('.');
    end
end
fprintf('\n');

