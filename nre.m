function n = nre(X)
% normwise relative error of an interval
n = mag(norm(rad(X),'fro') / norm(X,'fro'));
