function [XX, k, stab] = verifycare_fixedpoint_permuted(AA, GG, QQ)
% verify the stabilizing solution of an algebraic Riccati equation 
% A^*X + XA + Q = XGX using a fixed-point algorithm inspired by the ADI
% iteration and a change of basis inspired from [Mehrmann, Poloni 2012].
% Requires the Matlab package PGDoubling https://bitbucket.org/fph/pgdoubling
%
% [XX, k, stab] = verifycare(A, G, Q)
%
% Returns: XX (intval): an enclosure for a solution. This is guaranteed to
% be an enclosure for the unique stabilizing solution of the CARE if
% stab=1.
% k: number of iterations taken
% stab: result of stability verification. Return values are as in
% verhurwstab.m from Intlab: 1 means that stability is verified, 0 or -1
% means failure.
%
% % This function implements Method F from [Haqiri, Poloni, 2016+]

n = length(AA);
X = care_solution(AA, GG, QQ);

sym = symBasisFromSymplecticSubspace([eye(n);X]);
H = [AA -GG; -QQ -AA'];
Pi = rowSwap(eye(2*n), sym.v, 'N');

Hp = Pi * H * Pi';


Ap = Hp(1:n,1:n);
Gp = -Hp(1:n,n+1:end);
Qp = -Hp(n+1:end,1:n);

[Xp, k] = verifycare_fixedpoint_changebasis_mod(Ap, Gp, Qp);

% basis change, TODO: improve
U=[eye(size(Xp));Xp];
U=rowSwap(U,sym.v,'T');
XX = U(n+1:end,:) / U(1:n, :);

if nargout > 2
    stab = verhurwstab_miyajima(AA-GG*XX);
end
