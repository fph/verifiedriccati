function X = solveCARE_schur(A, B, C);
%solves a CARE with the algorithm from Miyajima's code Mn

NumMat = 3;
spTol = 0.1;

setround(0)
if isreal(A) == 0
    disp('Error: Mn (isreal(A) == 0)');  X = NaN;  Err = NaN;  return;
end
if isreal(B) == 0
    disp('Error: Mn (isreal(B) == 0)');  X = NaN;  Err = NaN;  return;
end
if isreal(C) == 0
    disp('Error: Mn (isreal(C) == 0)');  X = NaN;  Err = NaN;  return;
end

% Step 1
n = length(A);  X = schur_care(A,B,C);  X = (X + X')/2;  
if isreal(X) == 0
    disp('Error: Mn (isreal(X) == 0)');  Err = NaN;  return;
end

% Newton iteration (only once)
% extra-precise evaluations of the matrix multiplications
ATCell = Split_Mat(A',NumMat,spTol);  BCell = Split_Mat(B,NumMat,spTol);  
XCell = Split_Mat(-X,NumMat,spTol);  % "minus" X
if undfl(A',X) ~= 1
    disp('Error: Mn (undfl(A^T,X) ~= 1 in IR)');  Err = NaN;  return;
end
[EFATX,RestATX] = Acc_MulACell(ATCell,X,NumMat,spTol);  
EFXA = cell(1,length(EFATX));  
for inloop = 1:length(EFATX), EFXA{inloop} = EFATX{inloop}';  end;  
RestXA = cell(1,length(RestATX));  
for inloop = 1:length(RestATX), RestXA{inloop} = RestATX{inloop}';  end;  
if undfl(B,X) ~= 1
    disp('Error: Mn (undfl(B,X) ~= 1 in IR)');  Err = NaN;  setround(0);  return;
end
[EFBX,RestBX] = Acc_MulACell(BCell,X,NumMat,spTol);
if undfl(-X,EFBX{1}) ~= 1
    disp('Error: Mn (undfl(-X,EFBX{1}) ~= 1 in IR)');  Err = NaN;  setround(0);  return;
end
[EFXBX,RestXBX] = Acc_MulACell(XCell,EFBX{1},NumMat,spTol); 
for inloop = 2:length(EFBX),  EFBX{inloop} = -X*EFBX{inloop};  end  
for inloop = 1:length(RestBX),  RestBX{inloop} = -X*RestBX{inloop};  end  
FXCell = [EFATX,EFXA,EFXBX,C,EFBX(2:end),RestATX,RestXA,RestXBX,RestBX];  
clear EFATX EFXA EFXBX EFBX RestATX RestXA RestXBX RestBX;
FX = sum2Cell(FXCell);  clear FXCell;  Cr = lyap(A'-X*B,FX);  clear FX;  
X = X + Cr;  clear Cr;  % update of the approximate solution
if isreal(X) == 0
    disp('Error: Mn (isreal(X) == 0)');  Err = NaN;  return;
end
end

function res = undfl(A,B)
% res = 1 no underfrow
% res = 0 ?
% use in setround(0)

setround(0)
c1=max(max(abs(A)));  c2=max(max(abs(B)));
A(A==0)=c1;  B(B==0)=c2;
if all(min(ufp(A),[],2)*min(ufp(B))>0)
    res=1;
else
    res=0;
end
end

function res = ufp(A)
u = 2^(-53);  phi = (2*u)^-1 + 1;  q = phi.*A;  res = abs(q-(1-u).*q);
end

function D = Split_Mat(A,l,delta)
u = 2^-53;
[m,n] = size(A);
q = size(A,2); 
k = 1;
beta = ceil((-log2(u) + log2(q))/2);
D{1} = zeros(size(A));
while (k < l)
    mu = max(abs(A),[],2);  % mu(i) = max_1<=j<=q abs(A(i,j))
    if max(mu) == 0  % check A(i,j) = 0 for all (i,j)
        return;
    end
    w = 2.^(ceil(log2(mu)) + beta);
    S = repmat(w,1,q);  % S = w*e'
    D{k} = (A + S) - S;
    A = A - D{k};
    % Checking sparsity of D{k}
    if nnz(D{k}) < delta*m*n, D{k} = sparse(D{k}); end
    k = k + 1;
end
if k == l
    D{k} = A;
end
end

function [EF,iRest,sRest]  = Acc_MulACellInt(ACell,B,k,delta)
hA = length(ACell);
E = Split_Mat(B',k,delta);  hB = length(E);
for i = 1:hB, E{i} = E{i}'; end
l = 0;
for r = 1:min(hA,k-1)
    for s = 1:min(hB,k-1)
       if r+s <= k
           l = l + 1;
           EF{l} = ACell{r}*E{s};  % error-free
       end
    end
end
iRest = cell(1,hA);
setround(-1)
for r = 1:hA
    iF = zeros(size(B));
    for s = k-r+1:hB, iF = iF + E{s}; end
    iRest{r} = ACell{r}*iF;
end
clear iF;
sRest = cell(1,hA);
setround(+1)
for r = 1:hA
    sF = zeros(size(B));
    for s = k-r+1:hB, sF = sF + E{s}; end
    sRest{r} = ACell{r}*sF;
end
clear sF
setround(0)
end

function Res = sum2CellInf(P)  
setround(0)
for i = 2:length(P)
    %[P{i},P{i-1}] = TwoSum(P{i},P{i-1});         
    X = P{i} + P{i-1};  % x = a + b;
    Z = X - P{i};  % z = x - a;
    P{i-1} = (P{i} - (X - Z)) + (P{i-1} - Z);  % y = (a - (x-z)) + (b-z);
    P{i} = X;
end
clear X Z;
Res = P{1};
setround(-1)
for i = 2:length(P)-1
    Res = Res + P{i};
end
Res = Res + P{end};
setround(0)
end

function Res = sum2CellSup(P) 
setround(0)
for i = 2:length(P)
    %[P{i},P{i-1}] = TwoSum(P{i},P{i-1});         
    X = P{i} + P{i-1};  % x = a + b;
    Z = X - P{i};  % z = x - a;
    P{i-1} = (P{i} - (X - Z)) + (P{i-1} - Z);  % y = (a - (x-z)) + (b-z);
    P{i} = X;
end
clear X Z;
Res = P{1};
setround(+1)
for i = 2:length(P)-1
    Res = Res + P{i};
end
Res = Res + P{end};
setround(0)
end

function [EF,Rest]  = Acc_MulACell(ACell,B,k,delta)
hA = length(ACell);
E = Split_Mat(B',k,delta);  hB = length(E);
for i = 1:hB, E{i} = E{i}'; end
l = 0;
for r = 1:min(hA,k-1)
    for s = 1:min(hB,k-1)
       if r+s <= k
           l = l + 1;
           EF{l} = ACell{r}*E{s};  % error-free
       end
    end
end
Rest = cell(1,hA);
for r = 1:hA
    F = zeros(size(B));
    for s = k-r+1:hB, F = F + E{s}; end
    Rest{r} = ACell{r}*F;
end
end

function Res = sum2Cell(P) 
setround(0)
E = zeros(size(P{1}));
S = P{1};
for i = 2:length(P)   
    X = P{i} + S;  %[X,Y] = twosum(P{i},S); 
    Z = X - P{i};
    Y = (P{i} - (X - Z)) + (S - Z);
    E = E + Y;
    S = X;
end
clear X Y Z;
Res = S + E;
end
