function X = solveCARE_schur(A, B, C)
% Solves a CARE using the Schur method, and ensures that the result is a
% symmetric matrix

setround(0);
X = schur_care(A,B,C);  X = (X + X')/2;