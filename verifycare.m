function [XX, k, stab] = verifycare(AA, GG, QQ)
% verify the stabilizing solution of an algebraic Riccati equation 
% A^*X + XA + Q = XGX using the algorithm in [Hashemi, 2012]
%
% [XX, k, stab] = verifycare(A, G, Q)
%
% Returns: XX (intval): an enclosure for a solution. This is guaranteed to
% be an enclosure for the unique stabilizing solution of the CARE if
% stab=1.
% k: number of iterations taken
% stab: result of stability verification. Return values are as in
% verhurwstab.m from Intlab: 1 means that stability is verified, 0 or -1
% means failure.
%
% % This function implements Method H from [Haqiri, Poloni, 2016+]

n = length(AA);
Xcheck = care_solution(AA, GG, QQ);
[V,Lambda] = eig(AA-GG*Xcheck);
W = inv(V);
IW = verifylss(W, eye(n));
IV = verifylss(V, eye(n));
lambda = diag(Lambda);
D = conj(lambda) * ones(1,n) + ones(n,1) * lambda.';

Xcheck = intval(Xcheck);
Res = AA'*Xcheck + Xcheck*AA + QQ - Xcheck*GG*Xcheck;
G = IW'*Res*V;
H = G ./ D;
Z = -W' * H * IV;

Y = Z; k = 0;
while true
    k = k + 1;
    if k == 50
        error('intlab:VerificationFailed','Verification failed');
    end
    Y = hull(Y*infsup(0.9,1.1)+realmin*infsup(-1,1),0);
    %oldY = Y;
    X = krawczyk2(Y);
    if in0(X,Y)
        break
    end
    Y = X;
  %  Y = intersect(X, Z);
%    X1 = krawczyk2(Y);
%    if in0(X1, Y)
%        X = X1;
%        break
%    end
%    Y = intersect(oldY, X1);
end
XX = Xcheck + X;

if nargout > 2
    stab = verhurwstab_miyajima(AA-GG*XX);
end


function X = krawczyk2(Y)
    ZZ = IW' * Y  * V;
    P = W * (AA-GG*(Xcheck + Y)) * IW;
    Q = IV * (AA-GG*(Xcheck + Y)) * V;
    E = (Lambda-P)'*ZZ + ZZ*(Lambda-Q);
    N = E ./ D;
    U = W'*N*IV;
    X = Z + U;
end
%k
end
