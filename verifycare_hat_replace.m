function [XX, k, stab] = verifycare_hat_replace(AA, GG, QQ)
% verify the stabilizing solution of an algebraic Riccati equation 
% A^*X + XA + Q = XGX using using a Krawczyk-based method.
%
% [XX, k, stab] = verifycare(A, G, Q)
%
% Returns: XX (intval): an enclosure for a solution. This is guaranteed to
% be an enclosure for the unique stabilizing solution of the CARE if
% stab=1.
% k: number of iterations taken
% stab: result of stability verification. Return values are as in
% verhurwstab.m from Intlab: 1 means that stability is verified, 0 or -1
% means failure.

n = length(AA);
Xcheck = care_solution(AA, GG, QQ);
[V,Lambda] = eig(AA-GG*Xcheck);
W = inv(V);
IW = verifylss(W, eye(n));
IV = verifylss(V, eye(n));
lambda = diag(Lambda);
D = conj(lambda) * ones(1,n) + ones(n,1) * lambda.';

% the matrices appearing in R are kron(M1.',M2) and kron(N1.',N2).
M1 = V;
M2 = IW';
N1 = IV;
N2 = W';

Xcheck = intval(Xcheck);
Res = AA'*Xcheck + QQ + Xcheck* (AA - GG*Xcheck);
G = M2*Res*M1;
Hhat = -G ./ D;

Zhat = Hhat;
k = 0;

while true
    k = k + 1;
    if k == 50
        error('intlab:VerificationFailed','Verification failed');
    end
    ZhatInflated = hull(Zhat*infsup(0.9,1.1)+realmin*infsup(-1,1),0);
    Nhat = krawczyk2(ZhatInflated);
    Khat = Hhat + Nhat;
    if in0(Khat,ZhatInflated)
        sol = Khat;
        break
    end
    Zhat = Khat;
%    Zhat = intersect(ZhatInflated, Khat);
    
%     Zhat2 = intersect(ZhatInflated, Khat);
%     Nhat2 = krawczyk2(Zhat2);
%     Khat2 = Hhat + Nhat2;
%     if in0(Khat2,Zhat2)
%         sol = Khat2;
%         break
%     end
%     Zhat = intersect(ZhatInflated, Khat2);
end
XX = Xcheck + N2*sol*N1;
if nargout > 2
   stab = verhurwstab_miyajima(A-G*XX);
end

    function K2 = krawczyk2(Zhat)
        Z = N2*Zhat*N1;
        P = M2*(AA-GG*(Xcheck))'*N2; %variant with a thinner interval
        Q = N1*(AA-GG*(Xcheck+Z))*M1; 
        E = (Lambda'-P)*Zhat + Zhat*(Lambda-Q);
        K2 = E ./ D;
    end
end
