function [times,iterations,normalized_times3,normalized_times2,stabs] = experiments_scaled_carex(carexnumber,methods)
% Routine to make experiments on a scaled version of a problem in [Chu, Liu, Mehrmann]
%
% [times,iterations,normalized_times3,normalized_times2,stabs] = experiments_scaled_carex(carexnumber,methods)
%
% carexnumber: number of Carex experiment to scale. In the paper we used
% 15.
% methods: a cell array containing pointers to functions, e.g.
% methods = {@verifycare, @verifycare_hat_permuted_replace, @Mn_wrapper,
% @verifycare_fixedpoint_permuted}.

ns = ceil(logspace(1,3,30));
times = nan(length(ns),length(methods));
normalized_times3 = nan(length(ns),length(methods));
normalized_times2 = nan(length(ns),length(methods));
iterations = nan(length(ns),length(methods));
stabs = nan(length(ns),length(methods));

for i = 1:length(ns)
    n = ns(i);
    [A,G,Q] =  carex(carexnumber,n);
    for j = 1:length(methods)
        f = methods{j};
        try
            tic
            [IXX, k, stab] = f(A,G,Q);
            times(i,j) = toc;
            normalized_times3(i,j) = times(i,j) / n^3;
            normalized_times2(i,j) = times(i,j) / n^2;
            iterations(i,j) = k;
            stabs(i,j) = stab;
        catch err
            if strcmp(err.identifier,'intlab:VerificationFailed')
                continue
            else
                rethrow(err);
            end
        end
    end
    fprintf('(%d)',n);
end
fprintf('\n');
plot(normalized_times3);
legend(cellfun(@func2str, methods, 'UniformOutput', false));