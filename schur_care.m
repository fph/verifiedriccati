function X =schur_care( A,B,C )
% computes the solution of CARE associated to the left half plane using a Schur
% decomposition

H = [ A -B ; -C -A'];
[Q, T] = schur(H);
d = diag(T);
p = real(d) < 0;
[Qs, Ts] = ordschur(Q, T, p);
U1 = Qs(1:sum(p),1:sum(p));
U2 = Qs (sum(p)+1:end, 1:sum(p));
X = U2/U1;
end