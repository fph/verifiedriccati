function hs = verhurwstab_miyajima(M,V,D,W)
% verify Hurwitz stability of an interval matrix M
%
% hs = verhurwstab_miyajima(M)
% hs = verhurwstab_miyajima(M,V,D,W)
%
%
% Uses the algorithm in [Miyajima, 2015].
%
% Input: V,D,W (optional) are floating point matrices such that 
% M \approx V*D*W, V*W \approx I is an approximate eigendecomposition of M
%
% Output: returns 1 if the verification is successful, -1 if it is not
% (same syntax as verhurwstab.m from Intlab v6)

n = length(M);
if any(isnan(M(:)))
    hs = 0;
    return;
end
if not(exist('D','var')) || any(isnan(D(:)))
	[V,D] = eig(mid(M));
end
if not(exist('W','var')) || any(isnan(W(:)))
	W = inv(V);
end

R = W*(M*V - intval(V)*D );
R = mag(R);
S = eye (n) - intval(V)*W;
S = mag(S);

rnd = getround;
setround(1);

t = sum(S,2);
u = sum(R,2);

if max(t) >= 1,  hs = -1; 
else
   r = u + max(u ./ -(t-1))*t;   %we can't simplify -(t-1) to 1-t because we need the correct rounding mode to make sure everything is bounded in the right direction
   if max (real(diag(D)) + r ) < 0
       hs = 1;
   else
       hs = -1;
   end
end
setround(rnd);
end
